﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CommSub;
using log4net;
using Messages;
using Messages.RequestMessages;
using SharedObjects;

namespace CommSub.Conversations.ReceiverConversations
{
    public abstract class Receiver : Conversation
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(Receiver));

        private Routing _routingMessage;
        private Message _request;
        private int _fromProcessId;

        protected int FromProcessId { get { return _fromProcessId; } }

        public override void Execute(object context = null)
        {
            Done = false;
            Logger.DebugFormat("Start {0} Conversation", GetType().Name);

            if (IsEnvelopeValid(IncomingEnvelope, AllowedTypes))
            {
                if (!IsConversationStateValid())
                    Error = Error.Get(Error.StandardErrorNumbers.InvalidConversationSetup);
                else if (!IsProcessStateValid())
                    Error = Error.Get(Error.StandardErrorNumbers.InvalidProcessStateForConversation);
                else
                {
                    _routingMessage = IncomingEnvelope.Message as Routing;
                    if (_routingMessage != null)
                    {
                        _request = _routingMessage.InnerMessage as Message;
                        _fromProcessId = _routingMessage.FromProcessId;
                    }
                    else
                    {
                        _request = IncomingEnvelope.Message as Message;
                        _fromProcessId = _request.MsgId.Pid;
                    }

                    Logger.DebugFormat("Handle {0} message from {1}", _request.GetType().Name, _fromProcessId);

                    HandleRequest(_request);
                }
            }

            if (Error != null)
            {
                Logger.Warn(Error.Message);
                Process.ErrorHistory.Add(Error);
            }

            Done = true;
            Logger.DebugFormat("End {0}", GetType().Name);
        }

        /// <summary>
        /// This method returns an array of types of valid requests.  The concrete conversation must implement this method.
        /// </summary>
        protected abstract Type[] AllowedTypes { get; }

        /// <summary>
        /// This method should return true when the conversation object contains the necessary information to execute.
        /// Override this method in concrete specializations, when necessary.
        /// </summary>
        /// <returns>true is the conversation contains all of the necessary information</returns>
        protected virtual bool IsConversationStateValid() { return true; }

        /// <summary>
        /// This method should return true when the process is in a valid state for this conversation to execute.
        /// Override this method in concrete specializations, when necessary.
        /// </summary>
        /// <returns>true if the process is in a state where it is okay for this conversation to execute</returns>
        protected virtual bool IsProcessStateValid() { return true; }

        /// <summary>
        /// This method creates a reply to the request.  It must be implemented in the concrete conversation
        /// </summary>
        /// <returns></returns>
        protected abstract void HandleRequest(Message request);
    }
}
