﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Messages.RequestMessages;
using Messages.ReplyMessages;
using CommSub;
using CommSubTesting.Conversations.ResponderConversations;
using CommSubTesting.Conversations.InitiatorConversations;
using Messages;

namespace CommSubTesting
{
    [TestClass]
    public class ConversationFactoryTester
    {
        [TestMethod]
        public void ConversationFactory_TestProperties()
        {
            DummyCommProcess process = new DummyCommProcess() { AssignedProcessId = 1 };

            DummyConversationFactory convFactory = new DummyConversationFactory() { };
            convFactory.Initialize();

            Assert.IsNull(convFactory.DefaultMaxRetries);
            Assert.IsNull(convFactory.DefaultTimeout);
            Assert.IsNull(convFactory.Process);

            convFactory.DefaultMaxRetries = 10;
            convFactory.DefaultTimeout = 100;
            convFactory.Process = process;

            Assert.AreEqual(10, convFactory.DefaultMaxRetries);
            Assert.AreEqual(100, convFactory.DefaultTimeout);
            Assert.AreEqual(process, convFactory.Process);
        }

        [TestMethod]
        public void ConversationFactory_TestMessageCanStartConvesation()
        {
            DummyConversationFactory convFactory = new DummyConversationFactory();
            convFactory.Initialize();

            Assert.IsTrue(convFactory.IncomingMessageCanStartConversation(typeof(AliveRequest)));
            Assert.IsTrue(convFactory.IncomingMessageCanStartConversation(typeof(DummyRequest)));

            Assert.IsFalse(convFactory.IncomingMessageCanStartConversation(typeof(AllowanceAllocationRequest)));
            Assert.IsFalse(convFactory.IncomingMessageCanStartConversation(typeof(AllowanceDeliveryRequest)));
        }

        [TestMethod]
        public void ConversationFactory_TestCreateFromMessage()
        {
            DummyCommProcess process = new DummyCommProcess() { AssignedProcessId = 1 };

            DummyConversationFactory convFactory = new DummyConversationFactory()
            {
                DefaultMaxRetries = 10,
                DefaultTimeout = 100,
                Process = process
            };
            convFactory.Initialize();

            Envelope env = new Envelope() { Message = new Message() };
            Conversation conv = convFactory.CreateFromMessageType(typeof(AliveRequest), env);

            Assert.IsInstanceOfType(conv, typeof(DummyAliveConversation));
            Assert.AreEqual(env, conv.IncomingEnvelope);
            Assert.AreEqual(process, conv.Process);
            Assert.AreEqual(convFactory.DefaultTimeout, conv.Timeout);
            Assert.AreEqual(convFactory.DefaultMaxRetries, conv.MaxRetries);

            conv = convFactory.CreateFromMessageType(typeof(AllowanceAllocationRequest), env);
            Assert.IsNull(conv);

            conv = convFactory.CreateFromMessageType(null, env);
            Assert.IsNull(conv);
        }

        [TestMethod]
        public void ConversationFactory_TestCreateFromConversation()
        {
            DummyCommProcess process = new DummyCommProcess() { AssignedProcessId = 1 };

            DummyConversationFactory convFactory = new DummyConversationFactory()
            {
                DefaultMaxRetries = 10,
                DefaultTimeout = 100,
                Process = process
            };
            convFactory.Initialize();

            DummyAliveConversation conv = convFactory.CreateFromConversationType<DummyAliveConversation>();

            Assert.IsInstanceOfType(conv, typeof(DummyAliveConversation));
            Assert.IsNull(conv.IncomingEnvelope);
            Assert.AreEqual(process, conv.Process);
            Assert.AreEqual(convFactory.DefaultTimeout, conv.Timeout);
            Assert.AreEqual(convFactory.DefaultMaxRetries, conv.MaxRetries);

            Conversations.InitiatorConversations.SimpleRequestReply conv2 = convFactory.CreateFromConversationType<Conversations.InitiatorConversations.SimpleRequestReply>();
            Assert.IsNull(conv2);
        }

        [TestMethod]
        public void ConversationFactory_TestInitialize()
        {
            DummyConversationFactory convFactory = new DummyConversationFactory();

            Assert.IsFalse(convFactory.IncomingMessageCanStartConversation(typeof(AliveRequest)));
            Assert.IsFalse(convFactory.IncomingMessageCanStartConversation(typeof(AllowanceAllocationRequest)));

            convFactory.Initialize();

            Assert.IsTrue(convFactory.IncomingMessageCanStartConversation(typeof(AliveRequest)));
            Assert.IsFalse(convFactory.IncomingMessageCanStartConversation(typeof(AllowanceAllocationRequest)));
        }
    }
}
